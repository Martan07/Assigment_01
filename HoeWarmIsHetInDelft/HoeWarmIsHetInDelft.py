# -*- coding: utf-8 -*-
"""
Assigment 1:

Retrieves from the Internet the current temperature in Delft and prints it to standard output, rounded to degrees
Celsius. Obtain the information from http://www.weerindelft.nl/

Possibility: scrappy or beatiifulsoap or with selenium?
"""

import requests
from bs4 import BeautifulSoup

# URL where the  actual weather data is
from requests import HTTPError

URL = "https://weerindelft.nl/WU/55ajax-dashboard-testpage.php"


class Temperature:
    """
    Parsing/Printing/Holding actual weather information  from given URL
    """
    def __init__(self):
        self.__actual = ''
        self.__feelslike = ''
        self.__max = ''
        self.__min = ''

    def parse(self, url):
        if url and url.strip():
            try:
                resp = requests.get(url)

                soup_html = BeautifulSoup(resp.content, 'html5lib')
                # extract actual temperature data from given element
                self.__actual = soup_html.find('span', attrs={'class': 'ajax', 'id': 'ajaxtemp'}).text
                self.__feelslike = soup_html.find('span', attrs={'class': 'ajax', 'id': 'ajaxfeelslike'}).text
                self.__max = soup_html.find('span', attrs={'class': 'ajax', 'id': 'ajaxtempmax'}).text
                self.__min = soup_html.find('span', attrs={'class': 'ajax', 'id': 'ajaxtempmin'}).text
                # remove unwanted characters from
                self.__actual = ''.join(c for c in self.__actual if c not in "\t\n °C")
                self.__feelslike = ''.join(c for c in self.__feelslike if c not in "\t\n °C")
                self.__max = ''.join(c for c in self.__max if c not in "\t\n °C")
                self.__min = ''.join(c for c in self.__min if c not in "\t\n °C")
                return self
            except HTTPError as http_err:
                print(f'HTTP error occurred: {http_err}')
                exit(1)
            except Exception as err:
                print(f'Other error occurred: {err}')
                exit(1)
        else:
            # print('url parameter should not be null!!')
            raise Exception("parameter 'url' should not be empty or None!!")

    def print_actual_temperature(self):
        print(f'{self.__actual} degrees Celsius')


if __name__ == '__main__':
    try:
        temp = Temperature().parse(URL)
        temp.print_actual_temperature()
    except Exception as err:
        print(f'Something went wrong: \n {str(err)}')
        exit(1)