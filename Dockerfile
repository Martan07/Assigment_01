FROM python:3

MAINTAINER Rene Mjartan "rene.mjartan@outlook.com"

COPY ./requirements.txt /requirements.txt

WORKDIR /

RUN pip3 install -r requirements.txt

COPY . /

ENTRYPOINT [ "python3" ]

CMD [ "HoeWarmIsHetInDelft/HoeWarmIsHetInDelft.py" ]

